echo "[Install open-ssh]"
bash -c "sudo apt install openssh-server"
bash -c "sudo ufw allow ssh"
echo "[Finish Install open-ssh]"

echo "[Install rosbridge-suite]"
bash -c "sudo apt install ros-melodic-rosbridge-suite"
echo "[Finish Install rosbridge-suite]"

echo "[Install rosbridge-server]"
bash -c "sudo apt-get install ros-melodic-rosbridge-server"
bash -c "source /opt/ros/melodic/setup.bash"
echo "[Finish Install rosbridge-server]"


# Copy script file
echo "[Copy start script file]"
bash -c "sudo cp websocket-start.sh /home/"
echo "[Finish copy script file]"

# Copy service script file
echo "[Copy service file]"
bash -c "sudo cp dogu-websocket.service /etc/systemd/system/"
echo "[Finish copy service file]"

# Enable service
bash -c "sudo systemctl enable dogu-websocket.service"

bash -c "sudo systemctl start dogu-websocket.service"
echo "[Finish setup]"